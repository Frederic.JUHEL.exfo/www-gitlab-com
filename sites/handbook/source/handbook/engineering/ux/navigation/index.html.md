---
layout: handbook-page-toc
title: "GitLab Navigation"
description: "The UX team owns the navigation structures of the GitLab product. Please review this information if you plan to propose changes to GitLab navigation."
---

#### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The UX team owns the navigation structures of the GitLab product. Please review this information if you plan to propose changes to GitLab navigation.

## GitLab Navigation

Navigation refers to both how the GitLab application is organized and how that organization is presented to users to enable them to move around the application. Navigation is important, because it
affects the usability and discoverability of our products.

Navigation includes two distinct areas: 

* **Top bar**: The top horizontal navigation that includes global links to areas such as your todos, issues, and merge requests across projects and groups.
* **Left sidebar**: The contextual navigation that is dependent on the page being viewed.

More details regarding Navigation can be found in the [Pajamas documentation](https://design.gitlab.com/regions/navigation).

## Making changes to the Left Sidebar

Anyone can propose a navigation change by creating an issue using the [Navigation - Left Sidebar Proposals](https://gitlab.com/gitlab-org/gitlab/-/issues\new?issuable_template=Navigation%20-%20Left%20Sidebar%20Proposals) issue template. The product team who owns the feature is responsible for implementing the change.

The [Foundations](/handbook/product/categories/#foundations-group) team owns navigation overall, so you should engage them in your proposed change.

When proposing changes to menu items in the top or left navigation, the [Foundations Product Manager](/handbook/product/categories/#foundations-group) is the [DRI](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/#what-is-a-directly-responsible-individual) who must be engaged for approval. The DRI will consult Design and Research counterparts to consider the proposal in the context of existing and ongoing user research, impact to the UX of our overall navigation strategy, and business need. They will also engage UX and Product Management leadership for feedback, as needed.

[ux-guide]: https://docs.gitlab.com/ee/development/ux_guide/
[ux-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX
[ux-ready-label]: https://gitlab.com/groups/gitlab-org/issues?scope=all&state=opened&utf8=%E2%9C%93&label_name%5B%5D=UX+ready
[gitlab-design-project-readme]: https://gitlab.com/gitlab-org/gitlab-design/blob/master/README.md
[twitter-sheet]: https://docs.google.com/spreadsheets/d/1GDAUNujD1-eRYxAj4FIYbCyy8ltCwwIWqVTd9-gf4wA/edit
[everyone-designer]: https://library.gv.com/everyone-is-a-designer-get-over-it-501cc9a2f434
[pajamas]: https://design.gitlab.com
